package com.formative.formative20.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.formative.formative20.model.ModelPerson;

public interface ModelRepository extends CrudRepository<ModelPerson, Integer> {
	ModelPerson save(ModelPerson model);
	ModelPerson findById(int id);
	List<ModelPerson>findAll();

}
